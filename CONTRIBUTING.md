Contributing
============

If you would like to contribute for the FoundryVTT Core localization into the Portuguese language you can do so in one of the following ways:

1. Translating the strings to the Portuguese language in the [project](https://poeditor.com/join/project/wTD4YL81ps) created in POEditor.

2. Sending merge requests to this repository with the [pt-BR.json](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/blob/master/pt-BR/pt-BR.json) file with its translated strings.

3. Creating new [issues](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/) in this repository containing any new translation or error that you found.