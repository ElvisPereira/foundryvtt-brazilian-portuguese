[![Version: 0.3.1](https://img.shields.io/badge/Version-0.3.1-blue)](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese) [![Foundry version: >=0.5.0](https://img.shields.io/badge/FoundryVTT-%3E%3D0.5.0-brightgreen)](http://foundryvtt.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discordapp.com/invite/DDBZUDf) [![Translation](https://img.shields.io/endpoint?url=https%3A%2F%2Ffvttptbr.herokuapp.com%2Ftranslation)](https://poeditor.com/join/project/wTD4YL81ps)



FoundryVTT Brazilian (Portuguese)
=================================

## Português

Esse módulo adiciona a opção de selecionar o idioma Português (Brasil) no menu de configurações do [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop"). Ao selecionar essa opção você obterá a tradução de diversos aspectos da interface do programa.

Se encontrar algum erro na tradução ou tiver uma sugestão de algum termo melhor para ser usado em alguma parte dela você pode abrir uma nova [*issue*](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/issues) ou enviar uma mensagem para **Bellenus#5269** ou **Melithian539#0625** no Discord.

> Atualizado para funcionar com a versão 0.5.3 do FoundryVTT

### Instalação

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/raw/master/pt-BR/module.json`

Se essa opção não funcionar faça o download do arquivo [pt-BR.zip](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/-/jobs/artifacts/master/raw/pt-BR.zip?job=build "pt-BR.zip") e extraia o conteúdo dele dentro da pasta `Data/modules/`

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.


---




## English

This module adds the option to select the *Português (Brasil)* language from the [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop") settings menu. Selecting this option will translate various aspects of the program interface.

If you find a mistake in the translation or have a suggestion of a better term to use somewhere in it you can open a new [*issue*](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/ "issues") or send a message to **Bellenus#5269** or **Melithian539#0625** at Discord.

> Updated to work with version 0.5.3 of FoundryVTT

### Installation

In the `Add-On Modules` option click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/raw/master/pt-BR/module.json`

If this option does not work download the [pt-BR.zip](https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese/-/jobs/artifacts/master/raw/pt-BR.zip?job=build "pt-BR.zip") file and extract its contents into the `Data/modules/` folder

Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.